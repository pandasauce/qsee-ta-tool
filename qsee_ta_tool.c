#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <stdint.h>
#include <elf.h>
#include <unistd.h>

int process_32_bit(FILE * f_mdt);
int process_64_bit(FILE * f_mdt);
int cfileexists(const char* filename);

typedef struct {
    int merge;
    char * ta_name;
    size_t sz_ta_name;
    int fix_dts;
} run_parameters_t;

run_parameters_t run_parameters;

int parse_arguments(int argc, char const * argv[])
{
    int opt;

    while ((opt = getopt(argc, (char * const *) argv, "mf:d")) != -1) {
        switch (opt) {
        case 'm':
            run_parameters.merge = 1;
            break;
        case 'f':
            run_parameters.sz_ta_name = strlen(optarg) + 1;
            run_parameters.ta_name = malloc(run_parameters.sz_ta_name);
            strcpy(run_parameters.ta_name, optarg);
            break;
        case 'd':
            run_parameters.fix_dts = 1;
            break;
        default:
            return -1;
        }
    }

    if (run_parameters.ta_name == NULL) return -1;

    return 0;
}

int main(int argc, char const *argv[])
{
    int rv = 0;

    run_parameters.merge = 0;
    run_parameters.fix_dts = 0;
    run_parameters.ta_name = NULL;
    run_parameters.sz_ta_name = 0;

    rv = parse_arguments(argc, argv);

    if (rv != 0) {
        printf("Usage: ./qsee_ta_tool -f <trustlet_name>\n");
        printf("  Optional: -m to merge .bXX into .ELF, -d to fix DT_* entries\n");
        return -1;
    }

    char ta_header_name[run_parameters.sz_ta_name + 4];
    sprintf(ta_header_name, "%s%s", run_parameters.ta_name, ".mdt");

    if (!cfileexists(ta_header_name)) {
        printf("File %s does not exist\n", ta_header_name);
        return -1;
    }

    uint8_t bitness;
    FILE * f_mdt;

    f_mdt = fopen(ta_header_name, "rb");
    fseek(f_mdt, 4, SEEK_SET);
    fread(&bitness, 1, 1, f_mdt);

    if (bitness == 1) {
        printf("[+] Processing a 32-bit TA\n");
        rv = process_32_bit(f_mdt);
    } else if (bitness == 2) {
        printf("[-] Processing a 64-bit TA\n");
        rv = process_64_bit(f_mdt);
    } else {
        printf("[-] Could not determine trustlet bitness\n");
        rv = -1;
    }

    fclose(f_mdt);

    return rv;
}

// this is a terrible spaghetti function that could use some refactoring
int process_32_bit(FILE * f_mdt) {
    char ta_elf_name[run_parameters.sz_ta_name + 4];
    char next_pheader_name[run_parameters.sz_ta_name + 4];
    sprintf(ta_elf_name, "%s%s", run_parameters.ta_name, ".elf");

    Elf32_Ehdr elf_header;

    fseek(f_mdt, 0, SEEK_SET);
    fread(&elf_header, sizeof(Elf32_Ehdr), 1, f_mdt);

    // Count program headers
    printf("[+] Found %d program headers\n", elf_header.e_phnum);

    int dt_entries = 0;
    int issues_found = 0;

    Elf32_Phdr next_pheader;

    FILE * f_ph;
    FILE * f_out;
    if (run_parameters.merge) {
        f_out = fopen(ta_elf_name, "wb");
    }
    fseek(f_mdt, elf_header.e_phoff, SEEK_SET);
    for (int i = 0; i < elf_header.e_phnum; i++) {
        fread(&next_pheader, sizeof(Elf32_Phdr), 1, f_mdt);
        Elf32_Phdr * tmp_pheader_buf = malloc(next_pheader.p_filesz);

        sprintf(next_pheader_name, "%s.b%02d", run_parameters.ta_name, i);
        if (run_parameters.merge) {
            printf("[+] Merging %s into %s, size: 0x%08X, offset: 0x%08X\n",
                   next_pheader_name,
                   ta_elf_name,
                   next_pheader.p_filesz,
                   next_pheader.p_offset);
        } else {
            printf("[+] Processing %s, size: 0x%08X, offset: 0x%08X\n",
                   next_pheader_name,
                   next_pheader.p_filesz,
                   next_pheader.p_offset);
        }
        f_ph = fopen(next_pheader_name, "rb");

        fread(tmp_pheader_buf, next_pheader.p_filesz, 1, f_ph);
        if (run_parameters.merge && next_pheader.p_type != PT_DYNAMIC) {
            fseek(f_out, next_pheader.p_offset, SEEK_SET);
            fwrite(tmp_pheader_buf, next_pheader.p_filesz, 1, f_out);
        }

        if (next_pheader.p_type == PT_DYNAMIC) {
            printf("[*] PT_DYNAMIC found! Sanity checking for known issues\n");
            fseek(f_ph, 0, SEEK_SET);
            Elf32_Dyn dt_entry;
            while (1) {
                int dt_read_result = fread(&dt_entry, sizeof(Elf32_Dyn), 1, f_ph);
                dt_entries++;

                if (!dt_read_result || dt_entry.d_tag == DT_NULL) {
                    // can be improved
                    break;
                }

                if (dt_entry.d_tag == DT_HASH && !dt_entry.d_un.d_val) { printf("[!] DT_HASH zeroed out!\n"); issues_found++; }
                if (dt_entry.d_tag == DT_SYMTAB && !dt_entry.d_un.d_val) { printf("[!] DT_SYMTAB zeroed out!\n"); issues_found++; }
                if (dt_entry.d_tag == DT_STRTAB && !dt_entry.d_un.d_val) { printf("[!] DT_STRTAB zeroed out!\n"); issues_found++; }
            }

            if (!issues_found && run_parameters.merge) {
                fseek(f_out, next_pheader.p_offset, SEEK_SET);
                fwrite(tmp_pheader_buf, next_pheader.p_filesz, 1, f_out);
            } else if (issues_found && run_parameters.merge && run_parameters.fix_dts) {
                fseek(f_ph, 0, SEEK_SET);
                fseek(f_out, next_pheader.p_offset, SEEK_SET);
                for (int phdr_idx = 0; phdr_idx < dt_entries; phdr_idx++) {
                    int dt_read_result = fread(&dt_entry, sizeof(Elf32_Dyn), 1, f_ph);

                    if (!dt_read_result) {
                        printf("[!] Unexpected issue while reading DT entries for fixup!\n");
                        exit(-1);
                    }

                    if ((dt_entry.d_tag == DT_HASH || dt_entry.d_tag == DT_SYMTAB || dt_entry.d_tag == DT_STRTAB) && !dt_entry.d_un.d_val) {
                        continue;
                    }

                    fwrite(&dt_entry, sizeof(Elf32_Dyn), 1, f_out);
                }

                printf("[*] Some familiar issues were found, attempted to fix the output file\n");
            } else if (issues_found && (!run_parameters.merge || !run_parameters.fix_dts)) {
                printf("[!] Some familiar issues were found, this file will likely load as garbage!!!\n");
            }
        }

        free(tmp_pheader_buf);
        fclose(f_ph);
    }

    if (issues_found && run_parameters.merge && run_parameters.fix_dts) {
        int clean_dt_entries = dt_entries - issues_found;
        size_t new_sizes = sizeof(Elf32_Dyn) * clean_dt_entries;
        // assume b00 always same as mdt
        fseek(f_mdt, elf_header.e_phoff, SEEK_SET);
        fseek(f_out, elf_header.e_phoff, SEEK_SET);
        for (int i = 0; i < elf_header.e_phnum; i++) {
            fread(&next_pheader, sizeof(Elf32_Phdr), 1, f_mdt);
            if (next_pheader.p_type == PT_DYNAMIC) {
                next_pheader.p_filesz = new_sizes;
                next_pheader.p_memsz = new_sizes;
                fwrite(&next_pheader, sizeof(Elf32_Phdr), 1, f_out);
            } else {
                fseek(f_out, sizeof(Elf32_Phdr), SEEK_CUR);
            }
        }
    }

    if (run_parameters.merge) {
        fclose(f_out);
    }

    return 0;
}

// copypasta with 32->64
// haven't tested this
int process_64_bit(FILE * f_mdt) {
    char ta_elf_name[run_parameters.sz_ta_name + 4];
    char next_pheader_name[run_parameters.sz_ta_name + 4];
    sprintf(ta_elf_name, "%s%s", run_parameters.ta_name, ".elf");

    Elf64_Ehdr elf_header;

    fseek(f_mdt, 0, SEEK_SET);
    fread(&elf_header, sizeof(Elf64_Ehdr), 1, f_mdt);

    // Count program headers
    printf("[+] Found %d program headers\n", elf_header.e_phnum);

    int dt_entries = 0;
    int issues_found = 0;

    Elf64_Phdr next_pheader;

    FILE * f_ph;
    FILE * f_out;
    if (run_parameters.merge) {
        f_out = fopen(ta_elf_name, "wb");
    }
    fseek(f_mdt, elf_header.e_phoff, SEEK_SET);
    for (int i = 0; i < elf_header.e_phnum; i++) {
        fread(&next_pheader, sizeof(Elf64_Phdr), 1, f_mdt);
        Elf64_Phdr * tmp_pheader_buf = malloc(next_pheader.p_filesz);

        sprintf(next_pheader_name, "%s.b%02d", run_parameters.ta_name, i);
        if (run_parameters.merge) {
            printf("[+] Merging %s into %s, size: 0x%08lX, offset: 0x%08lX\n",
                   next_pheader_name,
                   ta_elf_name,
                   next_pheader.p_filesz,
                   next_pheader.p_offset);
        } else {
            printf("[+] Processing %s, size: 0x%08lX, offset: 0x%08lX\n",
                   next_pheader_name,
                   next_pheader.p_filesz,
                   next_pheader.p_offset);
        }
        f_ph = fopen(next_pheader_name, "rb");

        fread(tmp_pheader_buf, next_pheader.p_filesz, 1, f_ph);
        if (run_parameters.merge && next_pheader.p_type != PT_DYNAMIC) {
            fseek(f_out, next_pheader.p_offset, SEEK_SET);
            fwrite(tmp_pheader_buf, next_pheader.p_filesz, 1, f_out);
        }

        if (next_pheader.p_type == PT_DYNAMIC) {
            printf("[*] PT_DYNAMIC found! Sanity checking for known issues\n");
            fseek(f_ph, 0, SEEK_SET);
            Elf64_Dyn dt_entry;
            while (1) {
                int dt_read_result = fread(&dt_entry, sizeof(Elf64_Dyn), 1, f_ph);
                dt_entries++;

                if (!dt_read_result || dt_entry.d_tag == DT_NULL) {
                    // can be improved
                    break;
                }

                if (dt_entry.d_tag == DT_HASH && !dt_entry.d_un.d_val) { printf("[!] DT_HASH zeroed out!\n"); issues_found++; }
                if (dt_entry.d_tag == DT_SYMTAB && !dt_entry.d_un.d_val) { printf("[!] DT_SYMTAB zeroed out!\n"); issues_found++; }
                if (dt_entry.d_tag == DT_STRTAB && !dt_entry.d_un.d_val) { printf("[!] DT_STRTAB zeroed out!\n"); issues_found++; }
            }

            if (!issues_found && run_parameters.merge) {
                fseek(f_out, next_pheader.p_offset, SEEK_SET);
                fwrite(tmp_pheader_buf, next_pheader.p_filesz, 1, f_out);
            } else if (issues_found && run_parameters.merge && run_parameters.fix_dts) {
                fseek(f_ph, 0, SEEK_SET);
                fseek(f_out, next_pheader.p_offset, SEEK_SET);
                for (int phdr_idx = 0; phdr_idx < dt_entries; phdr_idx++) {
                    int dt_read_result = fread(&dt_entry, sizeof(Elf64_Dyn), 1, f_ph);

                    if (!dt_read_result) {
                        printf("[!] Unexpected issue while reading DT entries for fixup!\n");
                        exit(-1);
                    }

                    if ((dt_entry.d_tag == DT_HASH || dt_entry.d_tag == DT_SYMTAB || dt_entry.d_tag == DT_STRTAB) && !dt_entry.d_un.d_val) {
                        continue;
                    }

                    fwrite(&dt_entry, sizeof(Elf64_Dyn), 1, f_out);
                }

                printf("[*] Some familiar issues were found, attempted to fix the output file\n");
            } else if (issues_found && (!run_parameters.merge || !run_parameters.fix_dts)) {
                printf("[!] Some familiar issues were found, this file will likely load as garbage!!!\n");
            }
        }

        free(tmp_pheader_buf);
        fclose(f_ph);
    }

    if (issues_found && run_parameters.merge && run_parameters.fix_dts) {
        int clean_dt_entries = dt_entries - issues_found;
        size_t new_sizes = sizeof(Elf64_Dyn) * clean_dt_entries;
        // assume b00 always same as mdt
        fseek(f_mdt, elf_header.e_phoff, SEEK_SET);
        fseek(f_out, elf_header.e_phoff, SEEK_SET);
        for (int i = 0; i < elf_header.e_phnum; i++) {
            fread(&next_pheader, sizeof(Elf64_Phdr), 1, f_mdt);
            if (next_pheader.p_type == PT_DYNAMIC) {
                next_pheader.p_filesz = new_sizes;
                next_pheader.p_memsz = new_sizes;
                fwrite(&next_pheader, sizeof(Elf64_Phdr), 1, f_out);
            } else {
                fseek(f_out, sizeof(Elf64_Phdr), SEEK_CUR);
            }
        }
    }

    if (run_parameters.merge) {
        fclose(f_out);
    }

    return 0;
}

// https://www.zentut.com/c-tutorial/c-file-exists/
int cfileexists(const char* filename) {
    struct stat buffer;
    int exist = stat(filename, &buffer);
    if (exist == 0)
        return 1;
    else // -1
        return 0;
}