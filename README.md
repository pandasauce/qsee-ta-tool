A little "deobfuscation" tool I made for trustlets (trusted apps) running on Qualcomm's Secure Execution Environment (QSEE).

Some trustlets have some of their ELF Dynamic Tags reset to `0`. This has the effect of obfuscation, because it causes disassemblers (e.g. IDA Pro) to interpret bytes at the beginning of the file as something they're not and that throws off the entire auto-analysis, resulting in completely garbage symbols getting picked up.

I didn't have the time to figure out why this was done and why they would still run fine in QSEE. Could be how the stripping tool works in QSEE DK, or could be deliberate. For my purposes it would be enough if IDA loaded the basics correctly, even if symbol tables were missing.

This tool will cut out the nulled tags in the dynamic linking section of the target ELF, making it load cleanly in IDA Pro.

# Usage
```
./qsee_ta_tool -f <trustlet_name>
  Optional: -m to merge .bXX into .ELF, -d to fix DT_* entries
```